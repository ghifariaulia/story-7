$(function () {
    $('.up').on('click', function () {
        var item = $(this).closest('.card');
        item.insertBefore(item.prev());
    });
    $('.down').on('click', function () {
        var item = $(this).closest('.card');
        item.insertAfter(item.next());
    });
});
